all: xhtml

xhtml: html/debian-islamic-policy.html

html/debian-islamic-policy.html: debian-islamic-policy.xml
	mkdir -p html
	xmlto -o html/ xhtml-nochunks $<

distclean:
	rm -rf html/

.PHONY: xthml distclean
